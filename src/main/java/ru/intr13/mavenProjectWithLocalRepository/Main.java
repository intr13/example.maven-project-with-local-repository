package ru.intr13.mavenProjectWithLocalRepository;

import com.google.common.base.Function;

public class Main {

    public static void main(String[] args) {
        Function<String, String> function = new Function<String, String>() {
            @Override
            public String apply(String object) {
                return "Hello, " + object + "!";
            }
        };
        System.out.println(function.apply("world"));
    }
}
